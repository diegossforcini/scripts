<!DOCTYPE html>
<html>
	<head>
		<title>Calculadora</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="jquery/jquery-3.4.1.min.js"></script>
	</head>
	<style>
		* {
			box-sizing: border-box;
			font-family: monospace;
		}
		html, body {
			width: 100%;
			height: 100%;
			margin: 0;
			padding: 0px;
		}
		button {
			font-family: 'Calibri';
		}
		#fundo {
			width: 100%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
		}
		.calculadora {
			height: 600px;
			width: 400px;
			border: 0px solid #ccc;
			border-radius: 25px;
			padding: 10px;
			-webkit-box-shadow: 0px 0px 5px 3px rgba(0,0,0,0.3);
			-moz-box-shadow: 0px 0px 5px 3px rgba(0,0,0,0.3);
			box-shadow: 0px 0px 5px 3px rgba(0,0,0,0.3);
		}
		.calculadora .calculadora-interno {
			height: 100%;
			width: 100%;
			border: 2px solid #ccc;
			border-radius: 25px;
			overflow: hidden;
		}
		.calculadora .calculadora-interno .tela {
			background-color: rgb(250,250,250);
			height: 30%;
			border-bottom: 2px solid #ccc;
			display: flex;
			align-items: flex-end;
			justify-content: flex-end;
			flex-direction: column;
			font-size: 40px;
			padding: 5px;
		}
		.calculadora .calculadora-interno .tela .visor-cima,
		.calculadora .calculadora-interno .tela .visor {
			text-align: right;
		}
		.calculadora .calculadora-interno .tela .visor-cima {
			font-size: 25px;
			max-height: auto;
			overflow-y: auto;
		}
		.calculadora .calculadora-interno .tela .visor {
			overflow-x: auto;
			max-width: 100%;
			min-height: auto;
		}
		.calculadora .calculadora-interno .botoes {
			height: 70%;
			display: flex;
			flex-wrap: wrap;
		}
		.calculadora .calculadora-interno .botoes button.botao {
			cursor: pointer;
			font-size: 30px;
		}
		.calculadora .calculadora-interno .botoes .controles {
			flex: 0 0 100%;
			height: 20%;
			display: flex;
			flex-wrap: wrap;
		}
		.calculadora .calculadora-interno .botoes .controles button.botao {
			width: 50%;
		}
		.calculadora .calculadora-interno .botoes .numeros {
			flex: 0 0 80%;
			display: flex;
			flex-wrap: wrap;
			height: 80%;
		}
		.calculadora .calculadora-interno .botoes .numeros button.botao {
			width: 33.33333333333333%;
			display: inline-block;
		}
		.calculadora .calculadora-interno .botoes .numeros button.botao.botao-0 {
			width: 66.66666666666667%;
			display: inline-block;
		}
		.calculadora .calculadora-interno .botoes .numeros button.botao.botao-virgula {
			width: 33.33333333333333%;
			display: inline-block;
		}
		.calculadora .calculadora-interno .botoes .operadores {
			flex: 0 0 20%;
			height: 80%;
			display: flex;
			flex-wrap: wrap;
		}
		.calculadora .calculadora-interno .botoes .operadores button.botao {
			width: 100%;
		}
	</style>
	<body>
		<div id="fundo">
			<div class="calculadora">
				<div class="calculadora-interno">
					<div class="tela">
						<div class="visor-cima"></div>
						<div class="visor">0</div>
					</div>
					<div class="botoes">
						<div class="controles">
							<button type="button" class="botao botao-limpar">LIMPAR</button>
							<button type="button" class="botao botao-backspace"><--</button>
						</div>
						<div class="numeros">
							<button type="button" class="botao botao-numero botao-numero botao-7" valor="7">7</button>
							<button type="button" class="botao botao-numero botao-8" valor="8">8</button>
							<button type="button" class="botao botao-numero botao-9" valor="9">9</button>
							<button type="button" class="botao botao-numero botao-4" valor="4">4</button>
							<button type="button" class="botao botao-numero botao-5" valor="5">5</button>
							<button type="button" class="botao botao-numero botao-6" valor="6">6</button>
							<button type="button" class="botao botao-numero botao-1" valor="1">1</button>
							<button type="button" class="botao botao-numero botao-2" valor="2">2</button>
							<button type="button" class="botao botao-numero botao-3" valor="3">3</button>
							<button type="button" class="botao botao-numero botao-0" valor="0">0</button>
							<button type="button" class="botao botao-numero botao-virgula" valor=",">,</button>
						</div>
						<div class="operadores">
							<button type="button" class="botao botao-operacao botao-divisao" valor="/">/</button>
							<button type="button" class="botao botao-operacao botao-multiplicacao" valor="*">*</button>
							<button type="button" class="botao botao-operacao botao-subtracao" valor="-">-</button>
							<button type="button" class="botao botao-operacao botao-soma" valor="+">+</button>
							<button type="button" class="botao botao-operacao botao-igual" valor="=">=</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script>
		jQuery(document).ready(function($) {
			var valor_inicial = 0;
			var resetar = false;
			var total = 0;
			var texto_digitado = '';
			var novo_numero = false;
			var visor_cima = $('.tela .visor-cima');
			var visor = $('.tela .visor');
			var conta = '';

			$('.botao-limpar').on('click', function(event) {
				limpar_tudo();
			});

			$('.botao-backspace').on('click', function(event) {
				if (resetar) { limpar_tudo(); }

				texto_digitado = texto_digitado.slice(0, -1);
				visor.html(texto_digitado);
				console.log(texto_digitado.length);
				if (texto_digitado.length == 0) {
					visor.html(valor_inicial);
				}
			});

			$('.botao-numero').on('click', function(event) {
				if (resetar) { limpar_tudo(); }

				if ($(this).attr('valor') != ',') {
					if (novo_numero) {
						novo_numero = false;
						texto_digitado = $(this).attr('valor');
						visor.html(texto_digitado);
					} else {
						texto_digitado += $(this).attr('valor');
						visor.html(texto_digitado);
					}
				}
			});

			$('.botao-virgula').on('click', function(event) {
				if (resetar) { limpar_tudo(); }

				if (texto_digitado.indexOf(',') == -1) {
					if (texto_digitado.length == 0) {
						texto_digitado += '0' + $(this).attr('valor');
					} else {
						texto_digitado += $(this).attr('valor');
					}
					visor.html(texto_digitado);
				}
			});

			$('.botao-operacao').on('click', function(event) {
				if (resetar) { limpar_tudo(); }

				if (texto_digitado.length >= 1) {
					conta += ' ' + texto_digitado + ' ' + (($(this).attr('valor') != '=') ? $(this).attr('valor') : '');
					visor_cima.html(conta);
					novo_numero = true;
				}
			});

			$('.botao-operacao.botao-igual').on('click', function(event) {
				if (resetar) { limpar_tudo(); }

				let conta_sem_replace = conta;
				conta = conta.replace(/\s/g, '');
				conta = conta.replace(/\,/g, '.');
				total = eval(conta);
				texto_digitado = '';
				conta = '';
				visor_cima.html('');
				visor_cima.append(conta_sem_replace + ' = ' + total.toString().replace(/\./g, ','));
				visor.html(total.toString().replace(/\./g, ','));
				total = 0;
				resetar = true;
			});

			function limpar_tudo() {
				texto_digitado = '';
				conta = '';
				total = 0;
				visor_cima.html('');
				visor.html('');
				visor.html(valor_inicial);
				resetar = false;
			}
		});
	</script>
</html>