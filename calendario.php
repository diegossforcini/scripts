<!DOCTYPE html>
<html>
	<head>
		<title>Calendário</title>
	</head>
	<style>
		body, html {
			font-family: monospace;
			font-size: 15px;
		}
		.ano {
			text-align: center;
		}
		.ano .texto {
			position: relative;
			border-bottom: 1px solid #ccc;
			padding-bottom: 10px;
		}
		.mes {
			width: 33.33333333333333%;
			display: inline-block;
			margin-bottom: 20px;
		}
		.mes .mes-interno {
			display: flex;
			align-items: center;
			justify-content: center;
		}
		table {
		    border: 1px solid #ccc;
		}
		table th {
			padding: 7px;
		}
		table th.nome-mes {
			border: 1px solid #ccc;
		}
		table th.nome-mes .texto {
			margin: 0px;
		}
		table th.nome-semana {
			border: 1px solid #ccc;
		}
		table td {
			text-align: center;
			padding: 5px;
			border: 1px solid #ccc;
		}
		table td.dia-mes-posterior,
		table td.dia-mes-anterior {
			color: #ccc;
		}
	</style>
	<body>
		<?php
			$substituir = array(
				'proc' => array('Á', 'á', 'À', 'à', 'Â', 'â', 'Ã', 'ã', 'É', 'é', 'È', 'è', 'Ê', 'ê', 'Í', 'í', 'Ì', 'ì', 'Î', 'î', 'Ó', 'ó', 'Ò', 'ò', 'Õ', 'õ', 'Ô', 'ô', 'Ú', 'ú', 'Ù', 'ù', 'Û', 'û', 'Ç', 'ç', ' ', '/', '-', '+', '*', ',', '.', '[', ']', '(', ')', '<', '>', '{', '}', '=', '§', ';', ':', '¨', '¬', '&', '%', '$', '#', '@', '!', '?', '¹', '²', '³', '£', '¢', '\'', '"', '\\', '|', 'º', 'ª', '´', '`', '~', '^', 'Ñ', 'ñ'),
				'subs' => array('A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E', 'e', 'E', 'e', 'I', 'i', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'C', 'c', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_',  '_', '_',  '_', '_', '_', '_', '_', '_', '_', 'N', 'n'),
			);

			// echo diaSemana('2019-7-7');

			$ano = ((isset($_GET['ano']) && $_GET['ano'] != '') ? $_GET['ano'] : date('Y'));
			$meses = array(
				1 => array('nome' => 'JANEIRO', 'dias' => 31),
				2 => array('nome' => 'FEVEREIRO', 'dias' => (bissexto($ano) ? 29 : 28)),
				3 => array('nome' => 'MARÇO', 'dias' => 31),
				4 => array('nome' => 'ABRIL', 'dias' => 30),
				5 => array('nome' => 'MAIO', 'dias' => 31),
				6 => array('nome' => 'JUNHO', 'dias' => 30),
				7 => array('nome' => 'JULHO', 'dias' => 31),
				8 => array('nome' => 'AGOSTO', 'dias' => 31),
				9 => array('nome' => 'SETEMBRO', 'dias' => 30),
				10 => array('nome' => 'OUTUBRO', 'dias' => 31),
				11 => array('nome' => 'NOVEMBRO', 'dias' => 30),
				12 => array('nome' => 'DEZEMBRO', 'dias' => 31),
			);
			$semanas = array(
				1 => array('long' => 'DOMINGO', 'short' => 'DOM'),
				2 => array('long' => 'SEGUNDA', 'short' => 'SEG'),
				3 => array('long' => 'TERÇA', 'short' => 'TER'),
				4 => array('long' => 'QUARTA', 'short' => 'QUA'),
				5 => array('long' => 'QUINTA', 'short' => 'QUI'),
				6 => array('long' => 'SEXTA', 'short' => 'SEX'),
				7 => array('long' => 'SÁBADO', 'short' => 'SÁB'),
			);
			
			echo '<div class="ano"><h1 class="texto">'.$ano.'</h1></div>';

			foreach ($meses as $mes_index => $mes) {
				$mes_anterior = (($mes_index == 1) ? 12 : ($mes_index-1));
				$mes_posterior = (($mes_index == 12) ? 1 : ($mes_index+1));
				echo '<div class="mes mes-'.strtolower(str_replace($substituir['proc'], $substituir['subs'], $mes['nome'])).'">';
					echo '<div class="mes-interno">';
						echo '<table>';
							echo '<thead>';
								// echo '<tr>';
								// 	echo '<th class="nome-mes" colspan="7">'.$meses[$mes_anterior]['nome'].'</th>';
								// echo '</tr>';
								echo '<tr>';
									echo '<th class="nome-mes" colspan="7"><h2 class="texto">'.$mes['nome'].'</h2></th>';
								echo '</tr>';
								// echo '<tr>';
								// 	echo '<th class="nome-mes" colspan="7">'.$meses[$mes_posterior]['nome'].'</th>';
								// echo '</tr>';
								echo '<tr>';
									foreach ($semanas as $semana) {
										echo '<th class="nome-semana">'.$semana['short'].'</th>';
									}
								echo '</tr>';
							echo '</thead>';
							echo '<tbody>';
								$controlador_semana = 1;

								// percorrendo todos os dias do mês
								for ($dia = 1; $dia <= $mes['dias']; $dia++) {
									echo (($controlador_semana == 1) ? '<tr>' : '');

									$dia_atual = '<td class="dia-mes-atual">'.$dia.'</td>';

									// domingo
									if (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 1) {

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
											echo '<td class="dia-mes-posterior">2</td>';
											echo '<td class="dia-mes-posterior">3</td>';
											echo '<td class="dia-mes-posterior">4</td>';
											echo '<td class="dia-mes-posterior">5</td>';
											echo '<td class="dia-mes-posterior">6</td>';
										}
									}

									// segunda
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 2) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 2;
										}

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
											echo '<td class="dia-mes-posterior">2</td>';
											echo '<td class="dia-mes-posterior">3</td>';
											echo '<td class="dia-mes-posterior">4</td>';
											echo '<td class="dia-mes-posterior">5</td>';
										}
									}

									// terca
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 3) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-1).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 3;
										}

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
											echo '<td class="dia-mes-posterior">2</td>';
											echo '<td class="dia-mes-posterior">3</td>';
											echo '<td class="dia-mes-posterior">4</td>';
										}
									}

									// quarta
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 4) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-2).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-1).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 4;
										}

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
											echo '<td class="dia-mes-posterior">2</td>';
											echo '<td class="dia-mes-posterior">3</td>';
										}
									}

									// quinta
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 5) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-3).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-2).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-1).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 5;
										}

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
											echo '<td class="dia-mes-posterior">2</td>';
										}
									}

									// sexta
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 6) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-4).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-3).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-2).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-1).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 6;
										}

										echo $dia_atual; // dia atual do mes corrente

										if ($dia == $mes['dias']) {
											echo '<td class="dia-mes-posterior">1</td>';
										}
									}

									// sabado
									elseif (diaSemana($ano.'-'.$mes_index.'-'.$dia) == 7) {
										if ($dia == 1) {
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-5).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-4).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-3).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-2).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']-1).'</td>';
											echo '<td class="dia-mes-anterior">'.($meses[$mes_anterior]['dias']).'</td>';
											$controlador_semana = 7;
										}

										echo $dia_atual; // dia atual do mes corrente
									}

									echo (($controlador_semana == 7) ? '</tr>' : '');
									$controlador_semana = (($controlador_semana == 7) ? 1 : ($controlador_semana + 1));
								}
							echo '</tbody>';
						echo '</table>';
					echo '</div>';
				echo '</div>';
			}

			function bissexto($ano) {
				if (($ano % 4 == 0) && (($ano % 100 != 0) || ($ano % 400 == 0))) {
					// echo 'BISSEXTO';
					return true;
				} else {
					// echo 'NAO BISSEXTO';
					return false;
				}
			}

			function diaSemana($data) {
				return (date('w', strtotime($data)) + 1);
			}
		?>
	</body>
</html>